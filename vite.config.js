import path from 'path';
import { defineConfig } from 'vite';
import injectHTML from 'vite-plugin-html-inject';

export default defineConfig({
    base: '',
    build: {
      outDir: '../dist'
    },
    root: path.resolve(__dirname, 'src'),
    resolve: {
      alias: {
        '~bootstrap': path.resolve(__dirname, 'node_modules/bootstrap'),
      }
    },
    server: {
      port: 8080,
      hot: true
    },
    plugins: [injectHTML()],
  })


