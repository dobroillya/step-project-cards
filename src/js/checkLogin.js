import getElement from "./helpers/getElement";
import addLogiLogoutBtns from "./localstorage/addLogiLogoutBtns.js";

import { loginStorage } from "./login";

function checkLogin() {

  addLogiLogoutBtns();
  const btnLogout = getElement("#btn-logout");
  if (loginStorage.session) {
    btnLogout.addEventListener("click", () => {
      loginStorage.session='';
      location.reload();
    });
  }
}

export default checkLogin;
