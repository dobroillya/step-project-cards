import { refs, urls } from "./refs";
import { fetchData } from "./api/fetchData";
import { storageFromApi } from "./listOfVisit";
import { Visit } from "./classes/Visit";
import { loginStorage } from "./login";

const { formFielsVisit } = refs;

function submitVisit(nameOfDoctor, myModal) {
  const onlyInputs = document.querySelectorAll("#form-field-visit input");
  const selectUrgency = document.querySelector("#form-field-visit #urgency");
  const checkStatus = document.querySelector("#form-field-visit #status");

  function collBackSubmit(e) {
    e.preventDefault();

    console.log(checkStatus.checked);
    const getBody = [...onlyInputs].reduce(
      (acc, next) => {
        return { ...acc, [next.id]: next.value };
      },
      { doctor: nameOfDoctor, urgency: selectUrgency.value }
    );

    const addStatus = { ...getBody, status: checkStatus.checked };

    console.log(addStatus);

    (async function () {
      const data = await fetchData(urls.creatCard, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${loginStorage.session}`,
        },
        body: JSON.stringify(addStatus),
      });
      storageFromApi.addToSession({ ...addStatus, id: data.id });
      Visit.renderList(storageFromApi.session, data.id);
    })();

    myModal.hide();
  }

  formFielsVisit.addEventListener("submit", collBackSubmit);
}

export default submitVisit;
