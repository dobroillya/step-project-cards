import "../scss/styles.scss";
import "./login.js";
import checkLogin from "./checkLogin";
import { urls, refs } from "./refs";
import "./classes/Modal";
import createVisit from "./createVisit";
import { storageFromApi } from "./listOfVisit";
import { Visit } from "./classes/Visit";
import { fetchData } from "./api/fetchData";
import { Modal } from "./classes/Modal";
import getElement from "./helpers/getElement";
import { visitMap } from "./createVisit";
import submitEdit from "./submitEdit";
import listVisitHandler from "./listOfVisit";

import imgUrlLogo from "../images/doctor-f-svgrepo-com.svg";

document.getElementById('hero-img').src = imgUrlLogo;

const { listVisit, formEdit, statusFilter, searchFilter, urgencyFilter } = refs;

listVisitHandler();
checkLogin();
createVisit();

listVisit.addEventListener("click", (e) => {
  if (e.target.dataset.id) {
    storageFromApi.removeFromStorage(e.target.dataset.id);
    Visit.renderList(storageFromApi.session);
    (async function () {
      await fetchData(
        `${urls.creatCard}/${e.target.dataset.id}`,
        {
          method: "DELETE",
          headers: {
            Authorization: `Bearer ce45654e-c22b-45fc-b68b-e31336754a65`,
          },
        },
        true,
        true
      );
    })();
  } else if (e.target.dataset.edit) {
    const itemId = e.target.dataset.edit;
    const itemToEdit = storageFromApi.getItem(itemId);

    const visitDoctorEdit = new visitMap[itemToEdit["doctor"]]();
    const regegForValidation = visitDoctorEdit.regexForFields();
    const types = visitDoctorEdit.typesFields();

    Visit.renderForm(
      visitDoctorEdit.additionLabels(),
      formEdit,
      { ...itemToEdit, btn: "edit" },
      regegForValidation,
      types
    );

    const myModalEdit = Modal.setModal(modalVisitEdit);
    myModalEdit.show();

    const formSubmitEdit = getElement("#form-visit-edit");

    function handlerCallBackEdit(e) {
      e.preventDefault();
      submitEdit(itemId, itemToEdit.doctor);
      formSubmitEdit.removeEventListener("submit", handlerCallBackEdit);
      myModalEdit.hide();
    }

    formSubmitEdit.addEventListener("submit", handlerCallBackEdit);
  }
});

//filter
statusFilter.addEventListener("change", (e) => {
  storageFromApi.sortStatus(e.target.value);
  Visit.renderList(storageFromApi.filter);
});

//searchFilter
urgencyFilter.addEventListener("change", (e) => {
  storageFromApi.sortUrgency(e.target.value);
  Visit.renderList(storageFromApi.filter);
});

searchFilter.addEventListener("input", (e) => {
  storageFromApi.serachText(e.target.value);
  Visit.renderList(storageFromApi.filter);
});
