import * as bootstrap from "bootstrap";

export class Modal extends bootstrap.Modal {
  constructor(element, config) {
    super(element, config);
  }
  hide() {
    super.hide();
  }

  static setModal(el) {
    return new Modal(el, { keyboard: true });
  }
}
