export default class Storage {
  #filter;
  #status;
  #urgency;
  #serachTerm;

  constructor(title) {
    this.title = title;
    this.#filter = [];
    this.#status = "all";
    this.#urgency = "Терміновість";
    this.#serachTerm = "";
  }

  get session() {
    return JSON.parse(sessionStorage.getItem(this.title));
  }

  set session(obj) {
    sessionStorage.setItem(this.title, JSON.stringify(obj));
  }

  get filter() {
    return this.#filter;
  }

  set filter(objToFilter) {
    this.#filter = objToFilter;
  }

  addToSession(item) {
    const arr = this.session;
    arr.push(item);
    this.session = arr;
  }

  editItem(item) {
    this.removeFromStorage(item.id);
    const arr = this.session;
    this.session = [item, ...arr];
  }

  removeFromStorage(itemId) {
    const arr = this.session;
    const delFromArr = arr.filter((el) => el.id !== +itemId);
    this.session = delFromArr;
  }

  getItem(itemId) {
    const arr = this.session;
    return arr.filter((el) => el.id == +itemId)[0];
  }

  sortStatus(item) {
    this.#status = item;
    this.filter = this.session;
    if (this.#serachTerm !== "") {
      this.filter = this.filter.filter(
        (el) =>
          el.purpose.toLowerCase().includes(this.#serachTerm.toLowerCase()) ||
          el.description.toLowerCase().includes(this.#serachTerm.toLowerCase())
      );
    }
    if (item !== "all") {
      this.filter = this.filter.filter((el) => String(el.status) === item);
    }
    if (this.#urgency !== "Терміновість") {
      this.filter = this.filter.filter((el) => el.urgency === this.#urgency);
    }
  }

  sortUrgency(item) {
    this.#urgency = item;
    this.filter = this.session;
    if (this.#serachTerm !== "") {
      this.filter = this.filter.filter(
        (el) =>
          el.purpose.toLowerCase().includes(this.#serachTerm.toLowerCase()) ||
          el.description.toLowerCase().includes(this.#serachTerm.toLowerCase())
      );
    }
    if (item !== "Терміновість") {
      this.filter = this.filter.filter((el) => el.urgency === item);
    }
    if (this.#status !== "all") {
      this.filter = this.filter.filter(
        (el) => String(el.status) === this.#status
      );
    }
  }

  serachText(text) {
    this.#serachTerm = text;
    const arr = this.session;
    if (this.#status === "all" && this.#urgency === "Терміновість") {
      this.filter = arr.filter(
        (el) =>
          el.purpose.toLowerCase().includes(text.toLowerCase()) ||
          el.description.toLowerCase().includes(text.toLowerCase())
      );
    } else if (this.#status === "all") {
      this.filter = arr.filter(
        (el) =>
          el.purpose.toLowerCase().includes(text.toLowerCase()) ||
          el.description
            .toLowerCase()
            .includes(text.toLowerCase() && this.#urgency === el.urgency)
      );
    } else if (this.#urgency === "Терміновість") {
      this.filter = arr.filter(
        (el) =>
          el.purpose.toLowerCase().includes(text.toLowerCase()) ||
          el.description
            .toLowerCase()
            .includes(text.toLowerCase() && String(el.status) === this.#status)
      );
    } else {
      this.filter = arr.filter(
        (el) =>
          el.purpose.toLowerCase().includes(text.toLowerCase()) ||
          el.description
            .toLowerCase()
            .includes(
              text.toLowerCase() &&
                String(el.status) === this.#status &&
                this.#urgency === el.urgency
            )
      );
    }
  }
}
