import imgUrlDanefrom from "../../images/pngwing.png";
import { refs } from "../refs";
const { listVisit } = refs;

export class Visit {
  constructor() {}

  regexForFields() {
    return {
      name: "^[А-Яа-яЁёЇїІіЄєҐґиИ,./ ']+$",
      secondName: "^[А-Яа-яЁёЇїІіЄєҐґиИ,./ ']+$",
      surname: "^[А-Яа-яЁёЇїІіЄєҐґиИ,./ ']+$",
      purpose: "^[А-Яа-яЁёЇїІіЄєҐґ'/ ]+$",
      description: "^[А-Яа-яЁёЇїІіЄєҐґ'.,/ ]+$",
      urgency: "Терміновість",
    };
  }

  typesFields() {
    return {
      name: "text",
      secondName: "text",
      surname: "text",
      purpose: "text",
      description: "text",
    };
  }

  labels() {
    return {
      status: "Візит відбувся",
      name: "Ім'я",
      secondName: "По батькові",
      surname: "Прізвище",
      purpose: "Ціль визіту",
      description: "Опис",
      urgency: "Терміновість",
    };
  }

  static renderForm(data, element, dataEdit = {}, regegForValidation, types) {
    //console.log(regegForValidation);
    element.innerHTML = "";
    const addFields = Object.keys(data)
      .map((el) => {
        if (el === "urgency") {
          return `
          <div class="mb-3">
          <label for="${el}" class="form-label">${data[el]}</label>
          <select id="${el}" class="form-select" required aria-label="Default select example">
            <option selected value="">Вибір:</option>
            <option value="Низька">Низька</option>
            <option value="Середня">Середня</option>
            <option value="Висока">Висока</option>
        </select>
        </div>`;
        } else if (el === "status") {
          return `
          <div class="mb-3 form-check">
            <label for="${el}" class="form-check-label">${data[el]}</label>
            <input type="checkbox" class="form-check-input"
            id="${el}"
            checked
            >
          </div>
        `;
        } else {
          return `
          <div class="mb-3">
            <label for="${el}" class="form-label">${data[el]}</label>
            <input type="${types[el]}" pattern="${
            regegForValidation[el]
          }" required class="form-control"
            id="${el}"
            title="Тільки літери"
            value="${dataEdit[el] || ""}">
          </div>
        `;
        }
      })
      .join("");

    const addBtn =
      addFields +
      `<button id="btn-submit-${dataEdit["btn"]}" type="submit" class="btn btn-primary">Submit</button>`;

    element.insertAdjacentHTML("afterbegin", addBtn);
  }

  static renderList(data, dataId) {
    listVisit.innerHTML = "";
    const renderData = data
      .map((el) => {
        return `
        <div class="card glass-background-card width-400">
            <div class="card-header d-flex justify-content-between align-items-center">
                <p class="m-0 fw-semibold">${el.name} ${el.secondName} ${
          el.surname
        }</p>
                <span class="${
                  el.urgency === "Висока"
                    ? `bg-danger`
                    : el.urgency === "Середня"
                    ? `bg-warning`
                    : `bg-success`
                }
                  urgent-width">
                </span>
                <button data-id='${
                  el.id || dataId
                }' type="button" class="btn-close" aria-label="Close"></button>
            </div>
            <div class="card-body position-relative">
                <p>${el.doctor}</p>
                ${
                  el.status
                    ? `<div class="position-absolute done">
                <img class="img-done" width="120" alt="done">
              </div>`
                    : ``
                }

                <p>
                <button class="btn btn-light" type="button" data-bs-toggle="collapse" data-bs-target="#colapse-${
                  el.id
                }" aria-expanded="false" aria-controls="colapse${el.id}">
                  Деталі візиту
                </button>
              </p>
              <div class="collapse" id="colapse-${el.id}">

                <ul class="card card-body overflow-hidden">
                <li class=""><b class='fw-semibold'>Опис:</b> ${
                  el.description
                }</li>
                <li class="li"><b class='fw-semibold'>Ціль визіту:</b> ${
                  el.purpose
                }</li>
                <li class=""><b class='fw-semibold'>Терміновість:</b> ${
                  el.urgency
                }</li>
              </ul>

              </div>


                <button id="btn-create-visit" class='btn btn-primary ms-auto' data-edit='${
                  el.id || dataId
                }'  class='btn btn-primary'>Редагувати</button>
            </div>
       </div>
        `;
      })
      .join("");
    listVisit.insertAdjacentHTML("afterbegin", renderData);
    document
      .querySelectorAll(".img-done")
      .forEach((el) => (el.src = imgUrlDanefrom));
  }
}

export class VisitCardiologist extends Visit {
  constructor() {
    super();
  }

  typesFields() {
    return {
      ...super.typesFields(),
      pressure: "text",
      indexWeigth: "number",
      illnesses: "text",
      age: "text",
    };
  }

  regexForFields() {
    return {
      ...super.regexForFields(),
      pressure: "^([6-9][0-9]|1[0-9][0-9]?)/(5[0-9]|[6-9][0-9]|1[0-5][0-9])$",
      indexWeigth: "^1[2-9]|[2-9][0-9]|1[0-7][0-9](.[0-9])?|180(.0)?$",
      illnesses: "^[А-Яа-яЁёЇїІіЄєҐґ'.,/ ]+$",
      age: "^[1-9][0-9]{0,1}$|^1[0-1][0-9]$|^120$",
    };
  }

  additionLabels() {
    return {
      ...super.labels(),
      pressure: "Тиск (фомат: верхнє/нижнє. Верхнє між 69-199, нижнє: 50-159)",
      indexWeigth: "Індекс ваги (12 до 180)",
      illnesses: "Перенесені захворювання",
      age: "Вік",
    };
  }
}

export class VisitDentist extends Visit {
  constructor() {
    super();
  }

  typesFields() {
    return {
      ...super.typesFields(),
      lastDate: "date",
    };
  }

  regexForFields() {
    return {
      ...super.regexForFields(),
      lastDate: "^[1-9]$",
    };
  }

  additionLabels() {
    return {
      ...super.labels(),
      lastDate: "Дата останнього відвідування",
    };
  }
}

export class VisitTherapist extends Visit {
  constructor() {
    super();
  }

  typesFields() {
    return {
      ...super.typesFields(),
      age: "text",
    };
  }

  regexForFields() {
    return {
      ...super.regexForFields(),
      age: "^[1-9][0-9]{0,1}$|^1[0-1][0-9]$|^120$",
    };
  }

  additionLabels() {
    return {
      ...super.labels(),
      age: "Вік",
    };
  }
}
