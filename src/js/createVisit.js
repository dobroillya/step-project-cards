import { Modal } from "./classes/Modal";
import {
  VisitTherapist,
  VisitCardiologist,
  VisitDentist,
  Visit,
} from "./classes/Visit";
import getElement from "./helpers/getElement";
import { refs } from "./refs";
import submitVisit from "./submitVisit";

export const visitMap = {
  Кардіолог: VisitCardiologist,
  Стоматолог: VisitDentist,
  Терапевт: VisitTherapist,
};

function createVisit() {
  const modalVisit = getElement("#modalVisit");

  const btnVisit = getElement("#btn-create-visit");

  const myModal = Modal.setModal(modalVisit);

  const selectDoctor = getElement("#select-doctor");
  selectDoctor.addEventListener("change", (e) => {
    const visitDoctor = new visitMap[e.target.value]();

    const labels = visitDoctor.additionLabels();

    const regegForValidation = visitDoctor.regexForFields();
    const types = visitDoctor.typesFields();

    Visit.renderForm(
      labels,
      refs.formFielsVisit,
      { btn: "visit" },
      regegForValidation,
      types
    );

    submitVisit(e.target.value, myModal);
  });
}

export default createVisit;
