import { fetchData } from "./api/fetchData";
import { urls, refs } from "./refs";
import Storage from "./classes/Storage";
import { Visit } from "./classes/Visit";
import { loginStorage } from "./login";

const { listVisit } = refs;

export const storageFromApi = new Storage("listOfVisit");

storageFromApi.session=[];


export default function listVisitHandler() {
  if (!loginStorage.session || (!storageFromApi.session ?? !storageFromApi.session.length)) {
    listVisit.textContent = "No items have been added";
  } else {

    (async function () {
      storageFromApi.session = await fetchData(urls.creatCard, {
        method: "GET",
        headers: {
          Authorization: `Bearer ${loginStorage.session}`,
        },
      });
      Visit.renderList(storageFromApi.session);
    })();
  }
}




