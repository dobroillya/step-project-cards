
import { storageFromApi } from "./listOfVisit";
import { Visit } from "./classes/Visit";
import { fetchData } from "./api/fetchData";
import { urls } from "./refs";
import { loginStorage } from "./login";

function submitEdit(itemId, doctor) {
    const onlyInputs = document.querySelectorAll("#form-visit-edit input");
    const selectUrgency = document.querySelector("#form-visit-edit #urgency");
    const checkStatus = document.querySelector("#form-visit-edit #status");

    const getBody = [...onlyInputs].reduce(
        (acc, next) => {
          return { ...acc, [next.id]: next.value || next.placeholder };
        },
        {id: +itemId, urgency: selectUrgency.value, doctor}
      );

      const addStatus = {...getBody, status: checkStatus.checked};

      //console.log(addStatus);

      (async function () {
        const data = await fetchData(`${urls.creatCard}/${addStatus.id}`, {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${loginStorage.session}`,
          },
          body: JSON.stringify(addStatus),
        });

      })();

        storageFromApi.editItem({...addStatus});
        Visit.renderList(storageFromApi.session, addStatus.id);

}

export default submitEdit;