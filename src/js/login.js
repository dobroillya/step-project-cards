import { refs, urls } from "./refs";
import { fetchData } from "./api/fetchData";
import checkLogin from "./checkLogin";
import { Modal } from "./classes/Modal";
import Storage from "./classes/Storage";
import listVisitHandler from "./listOfVisit";

const { loginForm, loginUsername, loginPassword, modalLogin, alertLogin } =
  refs;

export const loginStorage = new Storage("userToken");

const myModal = Modal.setModal(modalLogin);

loginForm.addEventListener("submit", async (e) => {
  e.preventDefault();
  alertLogin.innerHTML = "";

  if (loginUsername.value === "" || loginPassword.value === "") {
    alert("Ensure you input a value in both fields!");
  } else {
    const response = await fetchData(
      urls.login,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email: loginUsername.value,
          password: loginPassword.value,
        }),
      },
      false
    );
    if (!response) {
      alertLogin.insertAdjacentHTML(
        "afterbegin",
        `<div class="alert alert-warning mt-2" role="alert">
          Incorrect login or password!
        </div>`
      );
    } else {
      myModal.hide();

      loginStorage.session = response;

      loginUsername.value = "";
      loginPassword.value = "";

      listVisitHandler();

      checkLogin();
    }
  }
});
