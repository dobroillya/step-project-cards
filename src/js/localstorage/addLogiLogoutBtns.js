import { refs } from "../refs";
import { loginStorage } from "../login";
const { btnLogin, navbarNav, main } = refs;


function addLogiLogoutBtns() {

  if (loginStorage.session) {
    btnLogin.innerHTML = "";
    btnLogin.style.display = 'none';
    main.insertAdjacentHTML('afterbegin', `<div><button id="btn-create-visit" class='btn btn-primary ms-auto' data-bs-toggle="modal" data-bs-target="#modalVisit">Create visit</button><div>`)
    navbarNav.insertAdjacentHTML('beforeend', `<li class="nav-item"><button id="btn-logout" class='btn btn-warning ms-auto'>Logout</button></li>`)
    return true
} else {

   btnLogin.textContent='Login'
   btnLogin.style.display = 'flex';
}
}

export default addLogiLogoutBtns;
