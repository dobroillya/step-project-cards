import getElement from "./helpers/getElement"

export const refs = {
    loginForm: getElement('#login-form'),
    loginUsername: getElement('#inputEmail'),
    loginPassword: getElement('#inputPassword'),
    btnLogin: getElement('#btn-login'),
    alertLogin: getElement('.alert-incorrect'),
    modalLogin:getElement('#exampleModal'),
    navbarNav: getElement('.navbar-nav'),
    navBarList: getElement('.navbar-nav'),
    btnLogout: getElement('#btn-logout'),
    formFielsVisit: getElement('#form-field-visit'),
    listVisit: getElement('#list-visit'),
    formEdit: getElement("#form-visit-edit"),
    navBar: getElement(".navbar"),
    main: getElement(".main"),
    statusFilter: getElement("#status-filter"),
    searchFilter: getElement("#search-filter"),
    urgencyFilter: getElement("#urgency-filter"),

}

export const urls = {
    login: 'https://ajax.test-danit.com/api/v2/cards/login',
    creatCard: 'https://ajax.test-danit.com/api/v2/cards'
}




