export async function fetchData(url, options, json = true, del=false) {
  const response = await fetch(url, options);

  try {
    if (!response.ok) {
      throw new Error("response not ok");
    }
    if(del) {
      return
    }
    if (json) {
      return await response.json();
    } else {
      return await response.text();
    }
  } catch (error) {
    console.log(error);
  }
}
